// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class WayPointState : uint8;
class AWayPoint;
#ifdef AI_1_WayPoint_generated_h
#error "WayPoint.generated.h already included, missing '#pragma once' in WayPoint.h"
#endif
#define AI_1_WayPoint_generated_h

#define AI_1_Source_AI_1_WayPoint_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Update(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetState) \
	{ \
		P_GET_ENUM(WayPointState,Z_Param_newState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetState(WayPointState(Z_Param_newState)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(WayPointState*)Z_Param__Result=P_THIS->GetState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetDelay) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newDelay); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetDelay(Z_Param_newDelay); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDelayAmount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetDelayAmount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetNext) \
	{ \
		P_GET_OBJECT(AWayPoint,Z_Param_point); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setNext(Z_Param_point); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetPrevious) \
	{ \
		P_GET_OBJECT(AWayPoint,Z_Param_point); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetPrevious(Z_Param_point); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNext) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AWayPoint**)Z_Param__Result=P_THIS->GetNext(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetPrevious) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AWayPoint**)Z_Param__Result=P_THIS->GetPrevious(); \
		P_NATIVE_END; \
	}


#define AI_1_Source_AI_1_WayPoint_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdate) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Update(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetState) \
	{ \
		P_GET_ENUM(WayPointState,Z_Param_newState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetState(WayPointState(Z_Param_newState)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(WayPointState*)Z_Param__Result=P_THIS->GetState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetDelay) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_newDelay); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetDelay(Z_Param_newDelay); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetDelayAmount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetDelayAmount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetNext) \
	{ \
		P_GET_OBJECT(AWayPoint,Z_Param_point); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setNext(Z_Param_point); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetPrevious) \
	{ \
		P_GET_OBJECT(AWayPoint,Z_Param_point); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetPrevious(Z_Param_point); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNext) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AWayPoint**)Z_Param__Result=P_THIS->GetNext(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetPrevious) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(AWayPoint**)Z_Param__Result=P_THIS->GetPrevious(); \
		P_NATIVE_END; \
	}


#define AI_1_Source_AI_1_WayPoint_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWayPoint(); \
	friend struct Z_Construct_UClass_AWayPoint_Statics; \
public: \
	DECLARE_CLASS(AWayPoint, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AI_1"), NO_API) \
	DECLARE_SERIALIZER(AWayPoint)


#define AI_1_Source_AI_1_WayPoint_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAWayPoint(); \
	friend struct Z_Construct_UClass_AWayPoint_Statics; \
public: \
	DECLARE_CLASS(AWayPoint, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AI_1"), NO_API) \
	DECLARE_SERIALIZER(AWayPoint)


#define AI_1_Source_AI_1_WayPoint_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWayPoint(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWayPoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWayPoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWayPoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWayPoint(AWayPoint&&); \
	NO_API AWayPoint(const AWayPoint&); \
public:


#define AI_1_Source_AI_1_WayPoint_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWayPoint(AWayPoint&&); \
	NO_API AWayPoint(const AWayPoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWayPoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWayPoint); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWayPoint)


#define AI_1_Source_AI_1_WayPoint_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PreviousWayPoint() { return STRUCT_OFFSET(AWayPoint, PreviousWayPoint); } \
	FORCEINLINE static uint32 __PPO__NextWayPoint() { return STRUCT_OFFSET(AWayPoint, NextWayPoint); } \
	FORCEINLINE static uint32 __PPO__State() { return STRUCT_OFFSET(AWayPoint, State); } \
	FORCEINLINE static uint32 __PPO__Arrow() { return STRUCT_OFFSET(AWayPoint, Arrow); }


#define AI_1_Source_AI_1_WayPoint_h_18_PROLOG
#define AI_1_Source_AI_1_WayPoint_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AI_1_Source_AI_1_WayPoint_h_21_PRIVATE_PROPERTY_OFFSET \
	AI_1_Source_AI_1_WayPoint_h_21_RPC_WRAPPERS \
	AI_1_Source_AI_1_WayPoint_h_21_INCLASS \
	AI_1_Source_AI_1_WayPoint_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AI_1_Source_AI_1_WayPoint_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AI_1_Source_AI_1_WayPoint_h_21_PRIVATE_PROPERTY_OFFSET \
	AI_1_Source_AI_1_WayPoint_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	AI_1_Source_AI_1_WayPoint_h_21_INCLASS_NO_PURE_DECLS \
	AI_1_Source_AI_1_WayPoint_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AI_1_API UClass* StaticClass<class AWayPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AI_1_Source_AI_1_WayPoint_h


#define FOREACH_ENUM_WAYPOINTSTATE(op) \
	op(WayPointState::Start) \
	op(WayPointState::Point) \
	op(WayPointState::End) \
	op(WayPointState::NotYetImplemented) 

enum class WayPointState : uint8;
template<> AI_1_API UEnum* StaticEnum<WayPointState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
