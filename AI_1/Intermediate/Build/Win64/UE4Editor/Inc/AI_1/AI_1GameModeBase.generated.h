// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AI_1_AI_1GameModeBase_generated_h
#error "AI_1GameModeBase.generated.h already included, missing '#pragma once' in AI_1GameModeBase.h"
#endif
#define AI_1_AI_1GameModeBase_generated_h

#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_RPC_WRAPPERS
#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAI_1GameModeBase(); \
	friend struct Z_Construct_UClass_AAI_1GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAI_1GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AI_1"), NO_API) \
	DECLARE_SERIALIZER(AAI_1GameModeBase)


#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAI_1GameModeBase(); \
	friend struct Z_Construct_UClass_AAI_1GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAI_1GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AI_1"), NO_API) \
	DECLARE_SERIALIZER(AAI_1GameModeBase)


#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAI_1GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAI_1GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAI_1GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAI_1GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAI_1GameModeBase(AAI_1GameModeBase&&); \
	NO_API AAI_1GameModeBase(const AAI_1GameModeBase&); \
public:


#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAI_1GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAI_1GameModeBase(AAI_1GameModeBase&&); \
	NO_API AAI_1GameModeBase(const AAI_1GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAI_1GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAI_1GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAI_1GameModeBase)


#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define AI_1_Source_AI_1_AI_1GameModeBase_h_12_PROLOG
#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_RPC_WRAPPERS \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_INCLASS \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AI_1_Source_AI_1_AI_1GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	AI_1_Source_AI_1_AI_1GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AI_1_API UClass* StaticClass<class AAI_1GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AI_1_Source_AI_1_AI_1GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
