// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AI_1/WayPoint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWayPoint() {}
// Cross Module References
	AI_1_API UEnum* Z_Construct_UEnum_AI_1_WayPointState();
	UPackage* Z_Construct_UPackage__Script_AI_1();
	AI_1_API UClass* Z_Construct_UClass_AWayPoint_NoRegister();
	AI_1_API UClass* Z_Construct_UClass_AWayPoint();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_GetDelayAmount();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_GetNext();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_GetPrevious();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_GetState();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_SetDelay();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_setNext();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_SetPrevious();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_SetState();
	AI_1_API UFunction* Z_Construct_UFunction_AWayPoint_Update();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
// End Cross Module References
	static UEnum* WayPointState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AI_1_WayPointState, Z_Construct_UPackage__Script_AI_1(), TEXT("WayPointState"));
		}
		return Singleton;
	}
	template<> AI_1_API UEnum* StaticEnum<WayPointState>()
	{
		return WayPointState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_WayPointState(WayPointState_StaticEnum, TEXT("/Script/AI_1"), TEXT("WayPointState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AI_1_WayPointState_Hash() { return 2327299353U; }
	UEnum* Z_Construct_UEnum_AI_1_WayPointState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AI_1();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("WayPointState"), 0, Get_Z_Construct_UEnum_AI_1_WayPointState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "WayPointState::Start", (int64)WayPointState::Start },
				{ "WayPointState::Point", (int64)WayPointState::Point },
				{ "WayPointState::End", (int64)WayPointState::End },
				{ "WayPointState::NotYetImplemented", (int64)WayPointState::NotYetImplemented },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "End.DisplayName", "End" },
				{ "ModuleRelativePath", "WayPoint.h" },
				{ "NotYetImplemented.DisplayName", "NotYetImplemented" },
				{ "Point.DisplayName", "Point" },
				{ "Start.DisplayName", "Start" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AI_1,
				nullptr,
				"WayPointState",
				"WayPointState",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void AWayPoint::StaticRegisterNativesAWayPoint()
	{
		UClass* Class = AWayPoint::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDelayAmount", &AWayPoint::execGetDelayAmount },
			{ "GetNext", &AWayPoint::execGetNext },
			{ "GetPrevious", &AWayPoint::execGetPrevious },
			{ "GetState", &AWayPoint::execGetState },
			{ "SetDelay", &AWayPoint::execSetDelay },
			{ "setNext", &AWayPoint::execsetNext },
			{ "SetPrevious", &AWayPoint::execSetPrevious },
			{ "SetState", &AWayPoint::execSetState },
			{ "Update", &AWayPoint::execUpdate },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics
	{
		struct WayPoint_eventGetDelayAmount_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventGetDelayAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Delay" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Get delay" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "GetDelayAmount", sizeof(WayPoint_eventGetDelayAmount_Parms), Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_GetDelayAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_GetDelayAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_GetNext_Statics
	{
		struct WayPoint_eventGetNext_Parms
		{
			AWayPoint* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWayPoint_GetNext_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventGetNext_Parms, ReturnValue), Z_Construct_UClass_AWayPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_GetNext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_GetNext_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_GetNext_Statics::Function_MetaDataParams[] = {
		{ "Category", "WayPointInfo" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Get next waypoint" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_GetNext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "GetNext", sizeof(WayPoint_eventGetNext_Parms), Z_Construct_UFunction_AWayPoint_GetNext_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetNext_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_GetNext_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetNext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_GetNext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_GetNext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_GetPrevious_Statics
	{
		struct WayPoint_eventGetPrevious_Parms
		{
			AWayPoint* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventGetPrevious_Parms, ReturnValue), Z_Construct_UClass_AWayPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::Function_MetaDataParams[] = {
		{ "Category", "WayPointInfo" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Blueprint accessor functions\n\n Get previous waypoint" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "GetPrevious", sizeof(WayPoint_eventGetPrevious_Parms), Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_GetPrevious()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_GetPrevious_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_GetState_Statics
	{
		struct WayPoint_eventGetState_Parms
		{
			WayPointState ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AWayPoint_GetState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventGetState_Parms, ReturnValue), Z_Construct_UEnum_AI_1_WayPointState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AWayPoint_GetState_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_GetState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_GetState_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_GetState_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_GetState_Statics::Function_MetaDataParams[] = {
		{ "Category", "WayPointState" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Get state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_GetState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "GetState", sizeof(WayPoint_eventGetState_Parms), Z_Construct_UFunction_AWayPoint_GetState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_GetState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_GetState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_GetState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_GetState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_SetDelay_Statics
	{
		struct WayPoint_eventSetDelay_Parms
		{
			float newDelay;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_newDelay;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWayPoint_SetDelay_Statics::NewProp_newDelay = { "newDelay", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventSetDelay_Parms, newDelay), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_SetDelay_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_SetDelay_Statics::NewProp_newDelay,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_SetDelay_Statics::Function_MetaDataParams[] = {
		{ "Category", "Delay" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Set delay" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_SetDelay_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "SetDelay", sizeof(WayPoint_eventSetDelay_Parms), Z_Construct_UFunction_AWayPoint_SetDelay_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_SetDelay_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_SetDelay_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_SetDelay_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_SetDelay()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_SetDelay_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_setNext_Statics
	{
		struct WayPoint_eventsetNext_Parms
		{
			AWayPoint* point;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_point;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWayPoint_setNext_Statics::NewProp_point = { "point", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventsetNext_Parms, point), Z_Construct_UClass_AWayPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_setNext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_setNext_Statics::NewProp_point,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_setNext_Statics::Function_MetaDataParams[] = {
		{ "Category", "WayPointInfo" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Set next waypoint" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_setNext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "setNext", sizeof(WayPoint_eventsetNext_Parms), Z_Construct_UFunction_AWayPoint_setNext_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_setNext_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_setNext_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_setNext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_setNext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_setNext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_SetPrevious_Statics
	{
		struct WayPoint_eventSetPrevious_Parms
		{
			AWayPoint* point;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_point;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::NewProp_point = { "point", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventSetPrevious_Parms, point), Z_Construct_UClass_AWayPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::NewProp_point,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::Function_MetaDataParams[] = {
		{ "Category", "WayPointInfo" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Set previous waypoint" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "SetPrevious", sizeof(WayPoint_eventSetPrevious_Parms), Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_SetPrevious()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_SetPrevious_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_SetState_Statics
	{
		struct WayPoint_eventSetState_Parms
		{
			WayPointState newState;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_newState;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_newState_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AWayPoint_SetState_Statics::NewProp_newState = { "newState", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WayPoint_eventSetState_Parms, newState), Z_Construct_UEnum_AI_1_WayPointState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AWayPoint_SetState_Statics::NewProp_newState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWayPoint_SetState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_SetState_Statics::NewProp_newState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWayPoint_SetState_Statics::NewProp_newState_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_SetState_Statics::Function_MetaDataParams[] = {
		{ "Category", "WayPointState" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Set state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_SetState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "SetState", sizeof(WayPoint_eventSetState_Parms), Z_Construct_UFunction_AWayPoint_SetState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_SetState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_SetState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_SetState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_SetState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_SetState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWayPoint_Update_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWayPoint_Update_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Update function for waypoint data" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWayPoint_Update_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWayPoint, nullptr, "Update", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWayPoint_Update_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AWayPoint_Update_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWayPoint_Update()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWayPoint_Update_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWayPoint_NoRegister()
	{
		return AWayPoint::StaticClass();
	}
	struct Z_Construct_UClass_AWayPoint_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arrow_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Arrow;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NextWayPointEditable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NextWayPointEditable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_delay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_delay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_State_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_State;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_State_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NextWayPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NextWayPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousWayPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousWayPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWayPoint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_AI_1,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AWayPoint_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AWayPoint_GetDelayAmount, "GetDelayAmount" }, // 3694422320
		{ &Z_Construct_UFunction_AWayPoint_GetNext, "GetNext" }, // 3103791919
		{ &Z_Construct_UFunction_AWayPoint_GetPrevious, "GetPrevious" }, // 3313352764
		{ &Z_Construct_UFunction_AWayPoint_GetState, "GetState" }, // 2079780367
		{ &Z_Construct_UFunction_AWayPoint_SetDelay, "SetDelay" }, // 915114623
		{ &Z_Construct_UFunction_AWayPoint_setNext, "setNext" }, // 1727278881
		{ &Z_Construct_UFunction_AWayPoint_SetPrevious, "SetPrevious" }, // 282023000
		{ &Z_Construct_UFunction_AWayPoint_SetState, "SetState" }, // 2208836966
		{ &Z_Construct_UFunction_AWayPoint_Update, "Update" }, // 42341784
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWayPoint_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WayPoint.h" },
		{ "ModuleRelativePath", "WayPoint.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWayPoint_Statics::NewProp_Arrow_MetaData[] = {
		{ "Category", "Visuals" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WayPoint.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWayPoint_Statics::NewProp_Arrow = { "Arrow", nullptr, (EPropertyFlags)0x00200800000b0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWayPoint, Arrow), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWayPoint_Statics::NewProp_Arrow_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::NewProp_Arrow_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPointEditable_MetaData[] = {
		{ "Category", "WayPointInfo" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Editor Editable Varibles, to be binded at the start of each game" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPointEditable = { "NextWayPointEditable", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWayPoint, NextWayPointEditable), Z_Construct_UClass_AWayPoint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPointEditable_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPointEditable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWayPoint_Statics::NewProp_delay_MetaData[] = {
		{ "Category", "WayPointInfo" },
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Set Delay amount" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWayPoint_Statics::NewProp_delay = { "delay", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWayPoint, delay), METADATA_PARAMS(Z_Construct_UClass_AWayPoint_Statics::NewProp_delay_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::NewProp_delay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWayPoint_Statics::NewProp_State_MetaData[] = {
		{ "Category", "WayPoint" },
		{ "ModuleRelativePath", "WayPoint.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AWayPoint_Statics::NewProp_State = { "State", nullptr, (EPropertyFlags)0x0040000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWayPoint, State), Z_Construct_UEnum_AI_1_WayPointState, METADATA_PARAMS(Z_Construct_UClass_AWayPoint_Statics::NewProp_State_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::NewProp_State_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AWayPoint_Statics::NewProp_State_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPoint_MetaData[] = {
		{ "ModuleRelativePath", "WayPoint.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPoint = { "NextWayPoint", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWayPoint, NextWayPoint), Z_Construct_UClass_AWayPoint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPoint_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWayPoint_Statics::NewProp_PreviousWayPoint_MetaData[] = {
		{ "ModuleRelativePath", "WayPoint.h" },
		{ "ToolTip", "Waypoint data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWayPoint_Statics::NewProp_PreviousWayPoint = { "PreviousWayPoint", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWayPoint, PreviousWayPoint), Z_Construct_UClass_AWayPoint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWayPoint_Statics::NewProp_PreviousWayPoint_MetaData, ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::NewProp_PreviousWayPoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWayPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWayPoint_Statics::NewProp_Arrow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPointEditable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWayPoint_Statics::NewProp_delay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWayPoint_Statics::NewProp_State,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWayPoint_Statics::NewProp_State_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWayPoint_Statics::NewProp_NextWayPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWayPoint_Statics::NewProp_PreviousWayPoint,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWayPoint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWayPoint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWayPoint_Statics::ClassParams = {
		&AWayPoint::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AWayPoint_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AWayPoint_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AWayPoint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWayPoint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWayPoint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWayPoint, 3006236822);
	template<> AI_1_API UClass* StaticClass<AWayPoint>()
	{
		return AWayPoint::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWayPoint(Z_Construct_UClass_AWayPoint, &AWayPoint::StaticClass, TEXT("/Script/AI_1"), TEXT("AWayPoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWayPoint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
