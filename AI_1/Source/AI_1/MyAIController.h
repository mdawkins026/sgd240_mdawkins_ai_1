// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MyAIController.generated.h"

UENUM(BlueprintType)
enum class MovementType : uint8
{
	Forward UMETA(DisplayName = "MoveForward"),
	Reverse UMETA(DisplayName = "MoveBackwards"),
	Loop UMETA(DisplayName = "MoveInALoop"),
	PingPong UMETA(DisplayName = "MovePingPong")
};

/**
 * 
 */
UCLASS()
class AI_1_API AMyAIController : public AAIController
{
	GENERATED_BODY()

		// Private Methods and vars 
private:
	// True if the Ai is moving
	UPROPERTY()
		bool isMovingProperty = false;
	UPROPERTY()
		bool isWaitingProperty = false;

	// An Array that stores all the way points that have been visited
	UPROPERTY()
		TArray<class AWayPoint*> VisitedPoints;

	// PingPong varibles
	UPROPERTY()
		class AWayPoint* PingPongA;
	UPROPERTY()
		class AWayPoint* PingPongB;

	// Method to check if the Ai is moving
	UFUNCTION()
		void CheckMoving();

	// Method for waiting, based on the waypoint delay amount
	UFUNCTION()
		void wait();
	// Deleget for timer, called when the timmer is finished
	UFUNCTION()
		void OnTimerEnd();
	//Timmer Handeller
	UPROPERTY()
		FTimerHandle WaitTimerHandle;

	// Sets the next target based on which movement type is selected
	UFUNCTION()
		void SetNextTarget();

	// Moves to the next waypoint
	UFUNCTION()
		void MoveToTarget();

	// Main Ai Loop
	UFUNCTION()
		void Main();

	//Finds the start waypoint and sets it (special condition if the movement type is set to reverse)
	UFUNCTION()
		void GetFirstTarget();

	// Public Methods and vars 
public:

	UPROPERTY(VisibleAnywhere, Category = "AiInfo")
		class AWayPoint* CurrentTarget;

	UPROPERTY(VisibleAnywhere, Category = "AiInfo")
		bool MovementStarted = false;

	UPROPERTY(VisibleAnywhere, Category = "AiInfo")
		bool MovementFinished = false;

	UPROPERTY(EditAnywhere, Category = "AiInfo")
		MovementType Movement = MovementType::PingPong;

	// Constructor
	AMyAIController();

	// Called at the start of the game. 
	UFUNCTION()
		virtual void BeginPlay() override;

	// Called once every tick
	UFUNCTION()
		virtual void Tick(float DeltaTime) override;

	// Method to Get if the Ai is moving
	UFUNCTION(BlueprintCallable, Category = "AiMehtods")
		bool IsMoving();

	// Method to check if the Ai is waiting
	UFUNCTION(BlueprintCallable, Category = "AiMehtods")
		bool IsWaiting();

	// Method to check if the Ai is at the desired target
	UFUNCTION(BlueprintCallable, Category = "AiMehtods")
		bool IsAtTarget();
};
