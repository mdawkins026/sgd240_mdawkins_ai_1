// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WayPoint.generated.h"

UENUM(BlueprintType)
enum class WayPointState : uint8
{
	Start UMETA(DisplayName = "Start"),
	Point UMETA(DisplayName = "Point"),
	End UMETA(DisplayName = "End"),
	NotYetImplemented UMETA(DisplayName = "NotYetImplemented")
};

UCLASS()
class AI_1_API AWayPoint : public AActor
{
	GENERATED_BODY()

// Default code
public:	
	// Sets default values for this actor's properties
	AWayPoint();	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when the item is placed
	virtual void OnConstruction(const FTransform & Transform) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// Waypoint data 
	UPROPERTY()
		AWayPoint* PreviousWayPoint;
	UPROPERTY()
		AWayPoint* NextWayPoint;
	UPROPERTY(VisibleAnywhere)
		WayPointState State;
	

public:
	// Set Delay amount
	UPROPERTY(EditAnywhere, Category = "WayPointInfo")
		float delay = 0;

	//Editor Editable Varibles, to be binded at the start of each game
	UPROPERTY(EditAnywhere, Category = "WayPointInfo")
		AWayPoint* NextWayPointEditable;

	//Blueprint accessor functions 
	//
	// Get previous waypoint
	UFUNCTION(BlueprintPure, Category = "WayPointInfo")
		AWayPoint* GetPrevious();

	// Get next waypoint
	UFUNCTION(BlueprintPure, Category = "WayPointInfo")
		AWayPoint* GetNext();

	// Set previous waypoint 
	UFUNCTION(Category = "WayPointInfo")
		void SetPrevious(AWayPoint* point);

	// Set next waypoint
	UFUNCTION(BlueprintCallable, Category = "WayPointInfo")
		void setNext(AWayPoint* point);

	// Get delay
	UFUNCTION(BlueprintPure, Category = "Delay")
		float GetDelayAmount();

	// Set delay
	UFUNCTION(BlueprintCallable, Category = "Delay")
		void SetDelay(float newDelay);

	// Get state
	UFUNCTION(BlueprintPure, Category = "WayPointState")
		WayPointState GetState();

	// Set state
	UFUNCTION(BlueprintCallable, Category = "WayPointState")
		void SetState(WayPointState newState);

private:
	// Update function for waypoint data
	UFUNCTION()
		void Update();

protected:
	UPROPERTY(VisibleDefaultsOnly, Category = "Visuals")
		class UArrowComponent* Arrow;
};
