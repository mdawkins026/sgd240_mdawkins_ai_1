// Fill out your copyright notice in the Description page of Project Settings.


#include "MyAIController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/AIModule/Classes/Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "WayPoint.h"
#include "EngineMinimal.h"
#include "Components/CapsuleComponent.h"


#pragma region BaseFunctionality
// Constructor function
AMyAIController::AMyAIController()
{
	// Enable the tick
	this->SetActorTickEnabled(true);

}

// Called at the start of the game
void AMyAIController::BeginPlay()
{
	Super::BeginPlay();

	GetFirstTarget();
}

// Called every tick
void AMyAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// If there is a current target
	if (CurrentTarget != nullptr)
	{
		Main();
	}
}
#pragma endregion

// Finds the first target for the AI's path
void AMyAIController::GetFirstTarget()
{
	//Get all the Waypoints
	TArray<AActor*> FoundActors;
	TSubclassOf<AActor> ClassToFind;
	ClassToFind = AWayPoint::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), ClassToFind, FoundActors);

	// if waypoints are found
	if (FoundActors.Num() != 0)
	{
		// Iterate through the found values and store
		for (size_t i = 0; i < FoundActors.Num(); i++)
		{
			AWayPoint* waypoint = (AWayPoint*)FoundActors[i];

			// if the movement is reverse set the target to the end point
			if (Movement == MovementType::Reverse)
			{
				if (waypoint->GetState() == WayPointState::End)
				{
					CurrentTarget = waypoint;
				}
			}
			// for all other movements set to start point 
			else
			{
				if (waypoint->GetState() == WayPointState::Start)
				{
					CurrentTarget = waypoint;
				}
			}
		}
	}
}

#pragma region Movement Code
// Retuns true when the Ai is moving
bool AMyAIController::IsMoving()
{
	return isMovingProperty;
}

// Checks to see if the player is moving then updates the vabrible
void AMyAIController::CheckMoving()
{
	FVector velocityVec = this->GetPawn()->GetVelocity();
	float velocity = velocityVec.Size();

	// Moving 
	if (velocity != 0.0f)
	{
		isMovingProperty = true;
	}
	// Not moving
	else if (velocity == 0.0f)
	{
		isMovingProperty = false;
	}
}

// Moves the AI to the target
void AMyAIController::MoveToTarget()
{
	FVector location = CurrentTarget->GetActorLocation();
	UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, location);
}
#pragma endregion

#pragma region Wait Code
// Gets the waiting state
bool AMyAIController::IsWaiting()
{
	return isWaitingProperty;
}

// starts the wait
void AMyAIController::wait()
{
	// Start the timmer
	GetWorld()->GetTimerManager().SetTimer(WaitTimerHandle, this, &AMyAIController::OnTimerEnd, CurrentTarget->GetDelayAmount(), false);
	isWaitingProperty = true;
}

// called when the timmer is up
void AMyAIController::OnTimerEnd()
{
	isWaitingProperty = false;
}
#pragma endregion

#pragma region Targert Code
//checks wheather the aI is at the target
bool AMyAIController::IsAtTarget()
{
	// Set the at target state to true when the AI is at idle
	if (GetMoveStatus() == EPathFollowingStatus::Idle)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Only Called when arrived at the targeted waypoint
// Stores the targeting methods for all movement types
void AMyAIController::SetNextTarget()
{
	// Next Target 
	AWayPoint* NextTarget;

	switch (Movement)
	{
		// Forward Targeting 
	case MovementType::Forward:

		// Get Next
		NextTarget = CurrentTarget->GetNext();

		// If the next point is the final point then set the movement to be finished
		if (NextTarget->GetState() == WayPointState::End)
		{
			MovementFinished = true;
		}

		//set the current target to the new target
		CurrentTarget = NextTarget;
		break;

		// Reverse Targeting
	case MovementType::Reverse:

		// Get Previous
		NextTarget = CurrentTarget->GetPrevious();

		// If the next point is the final point then set the movement to be finished
		if (NextTarget->GetState() == WayPointState::Start)
		{
			MovementFinished = true;
		}

		//set the current target to the new target
		CurrentTarget = NextTarget;
		break;

		// Loop Targeting
	case MovementType::Loop:
		// if the Ai is at the end of one cycle of the loop 
		if (CurrentTarget->GetState() == WayPointState::End)
		{
			for (size_t i = 0; i < VisitedPoints.Num(); i++)
			{
				AWayPoint* ittorated = VisitedPoints[i];
				if (ittorated->GetState() == WayPointState::Start)
				{
					CurrentTarget = ittorated;
				}
			}

			//Empty the visited point cache
			VisitedPoints.Empty();
		}
		// else move foward
		else
		{
			NextTarget = CurrentTarget->GetNext();
			CurrentTarget = NextTarget;
		}
		break;

		//Ping Pong Targeting
	case MovementType::PingPong:
		// Get the next target along the sequence
		NextTarget = CurrentTarget->GetNext();

		// this is first move set up varibles
		if (PingPongA == nullptr)
		{
			PingPongA = CurrentTarget;
			CurrentTarget = NextTarget;
			PingPongB = CurrentTarget;
			break;
		}
		else
		{
			//if the Ai is at the start point send it to the next target along the sequence
			if (CurrentTarget == PingPongA)
			{
				// if the next target to be moved to is the end, end the sequence after that
				if (PingPongB->GetState() == WayPointState::End)
				{
					MovementFinished = true;
				}
				CurrentTarget = PingPongB;
			}
			// if the Ai is a the next target move the target to the next valuen and send it back to the start
			else if (CurrentTarget == PingPongB)
			{
				PingPongB = NextTarget;
				CurrentTarget = PingPongA;
			}

			//Empty the visited point cache
			VisitedPoints.Empty();
			break;
		}
	}
}
#pragma endregion

// Called once per tick perfoms the main logical sequence for the AI
void AMyAIController::Main()
{
	//If the movement is not finished
	if (MovementFinished == false)
	{
		// Updates wheather or not the Ai is moving
		CheckMoving();

		// Primary Logic loop
		// S.T. (Stoped)^(At Target)^(Not Waiting) -> (move to next)
		//
		// Check if the AI is moving
		if (IsMoving() == false)
		{
			// Staring move 
			if (MovementStarted == false)
			{
				MoveToTarget();
				MovementStarted = true;
			}

			// Check if the AI at the Target
			if (IsAtTarget() == true)
			{
				// Check if the AI is Waiting
				if (IsWaiting() == false)
				{
					// If this is first time this loop is run, apply the wait 
					if (VisitedPoints.Contains(CurrentTarget) == false)
					{
						wait();
						VisitedPoints.Add(CurrentTarget);
					}
					else
					{
						SetNextTarget();
						MoveToTarget();
					}
				}
			}
		}
	}
}

