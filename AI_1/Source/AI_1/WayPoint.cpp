// Fill out your copyright notice in the Description page of Project Settings.


#include "WayPoint.h"
#include "Components/ArrowComponent.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"

// Sets default values
AWayPoint::AWayPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	//Create the arrow component
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	Arrow->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
	Arrow->SetVisibility(false, false);
}

// Called when the game starts or when spawned
void AWayPoint::BeginPlay()
{
	Super::BeginPlay();
}

//Called every time the actor is placed in the editor or moved.
void AWayPoint::OnConstruction(const FTransform & Transform)
{
	//Store the editable varible 
	NextWayPoint = NextWayPointEditable;

	//Update the points of the node
	Update();

	// Create an arrow if there is a next waypoint 
	if (NextWayPoint != nullptr)
	{
		//Arrow
		Arrow->SetVisibility(true, false);
		//Get the new rotation for the arrow
		FVector TargetLocation = NextWayPoint->GetActorLocation();
		FRotator ArrowRot = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), TargetLocation);

		//set the new rotation
		Arrow->SetRelativeRotation(ArrowRot);

		//Line
		DrawDebugLine(
			GetWorld(),
			this->K2_GetActorLocation(),
			NextWayPoint->K2_GetActorLocation(),
			FColor(255, 0, 0),
			false,
			10,
			0,
			1
		);
	}
}

// Called every frame
void AWayPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//Accessor functions
AWayPoint* AWayPoint::GetPrevious()
{
	return PreviousWayPoint;
}

AWayPoint* AWayPoint::GetNext()
{
	return NextWayPoint;
}

void AWayPoint::SetPrevious(AWayPoint* point)
{
	PreviousWayPoint = point;
}

void AWayPoint::setNext(AWayPoint* point)
{
	NextWayPoint = point;
}

float AWayPoint::GetDelayAmount()
{
	return delay;
}

void AWayPoint::SetDelay(float delay)
{
	this->delay = delay;
}

WayPointState AWayPoint::GetState()
{
	return State;
}

void AWayPoint::SetState(WayPointState state)
{
	this->State = state;
}

//Update function
void AWayPoint::Update()
{
	// Update function for way points

	// Logical statement
	if (NextWayPoint != nullptr) 
	{
		//set the next waypoints previous waypoint to this, if a next waypoint is supplied 
		NextWayPoint->SetPrevious(this);

		if (PreviousWayPoint != nullptr) 
		{
			// Waypoint is a Point node if it was both a next and previous waypoint
			SetState(WayPointState::Point);
		}
		else 
		{
			// Waypoint is a Start node if it has a next waypoint but not previous
			SetState(WayPointState::Start);
		}
	}
	else 
	{
		if (PreviousWayPoint != nullptr)
		{
			// Waypoint is an End node if it has no next waypoint but a previous 
			SetState(WayPointState::End);
		}
		else
		{
			// Waypoint is not yet implented if dose not have any data stored
			SetState(WayPointState::NotYetImplemented);
		}
	}
}
