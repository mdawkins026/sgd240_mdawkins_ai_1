# Spike Plan

# AI 1 – Navigation

## Context

We want to learn about the Artificial Intelligence systems that Unreal Engine has to offer. To get started, we want to do the basics – navigation.

## Grade Level

Pass

## Gap

1. Tech: How does the Unreal Engine NavMesh work?
2. Knowledge: What systems for navigation does Unreal Engine offer?
3. Skill: Creating [autonomous agents](https://en.wikipedia.org/wiki/Autonomous_agent) that can move!

## Goals/Deliverables

- The Spike Report should answer each of the Gap questions

Build a demo project with the following:

- Waypoints, a custom (C++) actor which:
  - Holds a reference to the next waypoint, which is set in the editor (Editable variable).
  - Holds a reference to the previous waypoint, which is set automatically
  - The next or previous node reference may be None/NULL, which means that the actor has reached the end of the waypoints, and should correctly stop (or handle the ending as per its Enum).
  - Each waypoint should have a (Editable) variable which controls how long any AI actor will wait at that point before continuing.
  - Create a Blueprint Child class, and:
    - Using Blueprint Construction Script, if the Next waypoint is set, go to that waypoint and set its previous Waypoint to be this waypoint.
    - Also, during the Construction Script, set up an Arrow Component to point in the direction of the next waypoint.
- Create a custom (blueprint) character which is correctly spawned with a custom (blueprint) AI controller.
  - Note: Do _not_ use a Behaviour Tree/Blackboard for this. Use simple blueprint nodes only.
  - It must have a (Editable, i.e. editable the level editor) variable, &quot;Path Mode&quot;, which is an Enum that you define and has the options &quot;Forward&quot;, &quot;Reverse&quot;, &quot;Loop&quot;, and &quot;Ping-Pong&quot;.
- Using a NavMesh Volume, the AI Controller must:
  - Navigate through a set of waypoints, moving to each one, waiting for the specified number of seconds, and then moving to the next one until the last one is reached.
  - You can have the character either be assigned the waypoint it will begin on (via an Editable variable), or have it find the nearest waypoint in the world to begin with.
  - Follow the waypoints based on the given Path Mode.
- Set up a level which showcases the following possible uses of your code:
  - An AI actor which follows a non-looped path to a dead-end, forward
  - An AI actor which follows a non-looped path to a dead-end, in reverse
  - An AI actor which follows a looped path (the last node points to the first node)
  - An AI actor which follows a non-looped path, and at the end moves directly back to the beginning (Loops, but without the last waypoint knowing about the first one)
  - An AI actor which follows a non-looped path in a &quot;ping-pong&quot; fashion

## Dates

| Planned start date: | Week 5 |
| --- | --- |
| Deadline: | Week 7 |

## Planning Notes

1. Read the [Unreal Engine documentation on NavMeshes](https://docs.unrealengine.com/latest/INT/Resources/ContentExamples/NavMesh/index.html)
2. Look at the [NavMesh content examples](https://docs.unrealengine.com/latest/INT/Resources/ContentExamples/NavMesh/index.html)
3. Find [some answers to common problems](https://answers.unrealengine.com/questions/22035/simple-move-to-location-problem.html)…