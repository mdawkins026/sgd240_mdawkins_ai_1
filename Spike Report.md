# Spike Report

# Ai- 1 Basic Ai navigation

## Introduction

The goal of this spike if to outline the process of basic AI navigation in unreal engine using C++, characters, AiControllers and NavMeshBounds. This will be achieved by Delivering on the deliverables outlined in the spike plan, to create a basic Ai character that moves around following waypoints.

## Goals

1. To deliver the spike deliverables as per the spike plan.
2. To gain an understanding of the basics if the Unreal Engine Ai system. Specifically gaining knowledge of:
  1. AiControler
  2. NavMeshBound
3. To stay within the scope of this spike by following the spike plan.

## Personnel

| Primary – Matthew Dawkins | Secondary – N/A |
| --- | --- |

## Technologies, Tools, and Resources used

- Comparing the location of two objects in unreal engine [https://forums.unrealengine.com/development-discussion/blueprint-visual-scripting/79816-compare-two-objects-position](https://forums.unrealengine.com/development-discussion/blueprint-visual-scripting/79816-compare-two-objects-position)
- Finding all actors of a specific class
  1. [https://answers.unrealengine.com/questions/289453/get-all-actors-of-class-in-c.html](https://answers.unrealengine.com/questions/289453/get-all-actors-of-class-in-c.html)
  2. [https://docs.unrealengine.com/en-US/API/Runtime/Engine/Kismet/UGameplayStatics/GetAllActorsOfClass/index.html](https://docs.unrealengine.com/en-US/API/Runtime/Engine/Kismet/UGameplayStatics/GetAllActorsOfClass/index.html)
- Tick function [https://docs.unrealengine.com/en-US/API/Runtime/AIModule/AAIController/Tick/index.html](https://docs.unrealengine.com/en-US/API/Runtime/AIModule/AAIController/Tick/index.html)
- Enums [https://wiki.unrealengine.com/Enums\_For\_Both\_C%2B%2B\_and\_BP](https://wiki.unrealengine.com/Enums_For_Both_C%2B%2B_and_BP)
- Logging [https://wiki.unrealengine.com/Logs,\_Printing\_Messages\_To\_Yourself\_During\_Runtime](https://wiki.unrealengine.com/Logs,_Printing_Messages_To_Yourself_During_Runtime)
- UPROPERTY [https://wiki.unrealengine.com/UPROPERTY](https://wiki.unrealengine.com/UPROPERTY)
- UFUNCTION [https://wiki.unrealengine.com/UFUNCTION](https://wiki.unrealengine.com/UFUNCTION)
- C++ switch [https://www.tutorialspoint.com/cplusplus/cpp\_switch\_statement.htm](https://www.tutorialspoint.com/cplusplus/cpp_switch_statement.htm)



## Tasks undertaken

In order to complete the spike three major Items must be implemented, the Waypoint actor (A custom actor that stores positions and links to one another), the AiControler class and the NavMeshVolume. This will be achieved by creating c++ classes with visuals added via blueprint.

**Waypoints**

_Create a new c++ class with AActor as the base class._

![1](reportImages\1.png)

_In the header create an enumerator to store the state of the waypoint_

![2](reportImages\2.png)

_Populate the header ensuring that the following properties are included with their associated getter and setter functions._

![](reportImages\3.png)

_After populating getter and setter functions, add the following functionality to the constructor to add a root component and to create the arrow that will point to the next waypoint._

![](reportImages\4.png)

_Next add a function that will update the arrows direction in the editor_

![](reportImages\5.png)

![](reportImages\6.png)





_Finally add an update function that updates all the data (previous and next waypoint) and sets the state of the waypoint from this._

![](reportImages\7.png)

_Adding some visuals to the way points by converting it to a blue print we get (where the waypoint on the bottom right is the start and the bottom left is the end._

![](reportImages\8.png)

![](reportImages\9.png)





**AiControler**

_In order to create an ai controller create a new C++ script with the AiControler as the super class, different implementations will implement the functionality differently so only the primary areas of complexity will be outlined_

**Getting the first target** _, in the header_

![](reportImages\10.png)

_Then in the cpp file, implement a function get all the waypoints then search for the starting point_

![](reportImages\11.png)

**Implementing movement**

_In order to move the pawn using the Navmesh we can utilize blueprint functions as they have the simplilest implantation._

![](reportImages\12.png)

**Selecting the next target**

_In the controller header file above the class, create an enum that stores what type of movement is to be used_

![](reportImages\13.png)

_Then in the class itself, create a variable that stores this data_

![](reportImages\14.png)

_From this we can then use a switch to define each of the targeting implementations in the cpp file_

_Forwards targeting_

![](reportImages\15.png)

_Backwards targeting_

![](reportImages\16.png)

_Loop Targeting_

![](reportImages\17.png)

_Ping pong targeting, requires two different target references thus in the header_

![](reportImages\18.png)

_Then in the switch statement_

![](reportImages\19.png)

**Main Logic loop**

_In order for the ai to follow the path set out by the way points the following function or loop is called every tick, in the cpp file_

![](reportImages\20.png)

_It is noted that some of the functions are not explained however their functionality is relatively self-explanatory. As seen in the comments the overarching logic statement is_

_or_



![](reportImages\20.png)



**NavMeshVolumes**

_For the ai to be able to determine a path towards the target waypoint we must include a NavMeshVolume that encompasses the area of play._

_First select the Nav Mesh Bounds Volume under volumes_

![](reportImages\21.png)

_Then edit the scale of the new volume to fit the play area_

![](reportImages\22.png)

_Finally select the nav mesh bound volume and then press p to enable it_

## What we found out

The primary area in which knowledge was gained was in the way in which the base level ue4 AI controller works. Specifically, how a controller can firstly find then pull information from a specific actor within the level. In this case the waypoint, lessons were learnt in how to effectively store and manage data to create the specified mechanic.

A area or struggle and thus learning was in implementing a c++ Enum data type as ue4 has api specific functionality and will not accept the core C++ code. Ultimately this was a relatively simple implementation, however finding up to date information on how to accomplish this proved challenging.

Some other areas where knowledge was gained include

- Basic state machines
- C++ switch statements
- Object pointers and dereferencing
- Ue4 C++ timers and delegate functions
- UGameplayStatistics
- How to debug nullptr exceptions that cause crashes of the ue4 editor
- Preventing nullptr crashes by implementing safe code

## Open Issues/risks

Initially I had planned on using the built-in navigation states to determine when the ai has stopped, however after several attempts this implantation was abandon for a dual state check. These being weather the ai is moving and whether the ai is at the target.

The use of the recursive main loop called every tick is a working solution however this is considerably inefficient. The use of event-based programing would greatly improve the efficiency and functionality of the ai.

A risk of the project lies in possibility of nullptr crashes, if there is any further development on this project is conducted.

## Recommendations

It is recommended further investigation of how this project could be competed using events instead of a tick loop. And on how to write safe code for ue4++ to prevent editor crashes.pi